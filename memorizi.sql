-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2016 at 02:35 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `memorizi`
--

-- --------------------------------------------------------

--
-- Table structure for table `diary_entries`
--

CREATE TABLE IF NOT EXISTS `diary_entries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diary_entries`
--

INSERT INTO `diary_entries` (`id`, `user_id`, `created_at`, `title`, `content`) VALUES
(2, 2, '2016-02-05 04:49:49', 'The girl', 'There was a girl named Becca and a boy named Joe. Becca was in a burning house. None of the firefighters could get in the house because the fire was too big. Joe dressed in one of the fire suits and got into the house. When he got up the stairs, the steps fell off behind him. When he got into her room he sealed the door up behind him. He held her tight, kissed her, huged her, then said that he loved her. She asked what was wrong, and he said that he was going to die. Her eyes widened as she began to cry. He picked her up and jumped out of the four story house. He landed on his back with her on top of him. He died to save her life.'),
(5, 5, '2016-02-05 10:08:32', 'Hello', 'this is adelle'),
(6, 6, '2016-02-06 09:26:30', 'Wrecking Ball', 'i came in like a wrecking ball.'),
(7, 7, '2016-02-15 17:01:41', 'love', 'yourself'),
(8, 7, '2016-02-15 19:50:55', 'i love u', 'yeahs'),
(9, 7, '2016-02-20 03:07:54', 'Lyn', 'Channnnnswann'),
(10, 7, '2016-02-20 03:45:40', 'Ajshdfkasfakh', 'Angelkhasdfmncm,bazxbvl'),
(11, 7, '2016-02-20 10:29:42', 'New Title', 'start writing here\r\nadfasfas'),
(12, 1, '2016-02-20 10:51:51', 'first entry', 'roar'),
(13, 2, '2016-03-05 08:19:54', 'Stay', 'I want you to stay\r\nNever go away from me\r\nStay forever\r\nBut now, now that you''re gone\r\nAll I can do is pray for you\r\nTo be here beside me again\r\n*Why did you have to leave me\r\nWhen you said that love will conquer all\r\nWhy did you have to leave me\r\nWhen you said that dreaming\r\nWas as good as reality\r\nAnd now I must move on\r\nTrying to forget all the memories\r\nOf you near me\r\nBut I can''t let go of your love\r\nThat has taught me to hold on\r\nI want you to stay never go away from me\r\nStay forever\r\nBut now, now that you''re gone\r\nAll I can do is pray for you\r\nTo be here beside me again\r\n(Repeat *1)\r\nAnd now I must move one\r\nTrying to forget all the memories\r\nOf you near me\r\nBut I can''t let go of your love\r\nThat has taught me to hold on\r\n(Repeat *twice)\r\nCoda:\r\nI want you to stay never go away from me\r\nStay forever\r\nI want to stay but I have to go my way\r\nOohhhmmm'),
(14, 14, '2016-03-05 08:22:36', 'Stay', 'I want you to stay\r\nNever go away from me\r\nStay forever\r\nBut now, now that you''re gone\r\nAll I can do is pray for you\r\nTo be here beside me again\r\n*Why did you have to leave me\r\nWhen you said that love will conquer all\r\nWhy did you have to leave me\r\nWhen you said that dreaming\r\nWas as good as reality\r\nAnd now I must move on\r\nTrying to forget all the memories\r\nOf you near me\r\nBut I can''t let go of your love\r\nThat has taught me to hold on\r\nI want you to stay never go away from me\r\nStay forever\r\nBut now, now that you''re gone\r\nAll I can do is pray for you\r\nTo be here beside me again\r\n(Repeat *1)\r\nAnd now I must move one\r\nTrying to forget all the memories\r\nOf you near me\r\nBut I can''t let go of your love\r\nThat has taught me to hold on\r\n(Repeat *twice)\r\nCoda:\r\nI want you to stay never go away from me\r\nStay forever\r\nI want to stay but I have to go my way\r\nOohhhmmm'),
(15, 14, '2016-03-05 08:28:20', 'Broken Hearted', 'I''ve been too much broken hearted, as in super my heart still aching. Until now tears and pain still remained in my heart! Fvck shit!'),
(16, 14, '2016-03-05 08:32:09', 'Never be fade', 'I''ll never forget you, because you''re the one who broke my heart. You hurt me too much! You don''t have conscience! You hurt me to much! I hate you!\r\n\r\n~broken hearted!'),
(17, 2, '2016-03-05 11:41:03', 'Somebody that I used to know', 'By GOtye');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `is_admin`) VALUES
(1, 'username', '5f4dcc3b5aa765d61d8327deb882cf99', 0),
(2, 'zhythero', '5f4dcc3b5aa765d61d8327deb882cf99', 1),
(3, 'angelyn', '5f4dcc3b5aa765d61d8327deb882cf99', 0),
(4, 'kelyn', '5f4dcc3b5aa765d61d8327deb882cf99', 0),
(5, 'aiko', '5f4dcc3b5aa765d61d8327deb882cf99', 0),
(6, 'kelyn4', '5f4dcc3b5aa765d61d8327deb882cf99', 0),
(7, 'lynnn', '3b6281fa2ce2b6c20669490ef4b026a4', 0),
(13, 'Jujuliet', '55ff3f31651c595fdf54120ec7a57b8e', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `diary_entries`
--
ALTER TABLE `diary_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_from_users` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diary_entries`
--
ALTER TABLE `diary_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
