<?php 

/**
* Only allow logged in users.
*
* @return void
*/
function only_logged_in() {
	if (!isset($_SESSION['user_id']))
		redirect('auth/login');
}

/**
* Only allow guests
*
* @return void
*/
function only_guests() {
	if (isset($_SESSION['user_id']))
		redirect('dashboard');
}

/**
* Only allow admins 
*
* @return void
*/
function only_admins() {
	if ($_SESSION['is_admin'] != 1)
		redirect('dashboard');
}

/**
* Returns true if the logged in user is admin
*
*@return boolean
*/
function is_admin() {

	return $_SESSION['is_admin'] == 1;
}

?>