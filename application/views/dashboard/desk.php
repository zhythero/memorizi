<script src="<?php echo base_url('angular/controllers/user/diaryEntriesController.js'); ?>"></script>

<section class="container-fluid" ng-controller="diaryEntriesController" ng-init="collection.get()" ng-cloak>

	<div class="row">

		<!-- Entries  -->
		<div class="col-md-9">

			<div class="well entry" data-toggle="modal" data-target="#modal-view-entry" ng-repeat="entry in collection.data" ng-click="selection.select(entry)">
				<h5 style="margin-bottom: 0">{{ entry.title }}</h5>
				<p><small><span am-time-ago="entry.created_at"></span></small></p>
			</div>

		</div>

		<!-- Options -->
		<div class="col-md-3">
			<button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#modal-compose"><i class="fa fa-pencil"></i> Compose</button>
		</div>

	</div>

	<!-- Modal: Compose -->
	<div class="modal fade" id="modal-compose">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4><i class="fa fa-pencil"></i> Write <small><em>How was your day?</em></small></h4>
				</div>
				<div class="modal-body">
					<p class="text-center" ng-class="{'text-danger' : compose.response.success == false, 'text-success' : compose.response.success == true}">{{ compose.response.message }}</p>
					<form action="" onsubmit="return false">
						<div class="form-group">
							<input type="text" class="form-control" ng-model="compose.data.title" placeholder="Title">
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="10" ng-model="compose.data.content" placeholder="Your thoughts here..."></textarea>
						</div>
						<button type="button" class="btn btn-primary" ng-click="compose.publish()" ng-disabled="compose.publishing">Publish</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</form>
				</div>
			</div>
		</div>

	</div>

	<!-- Modal: View Entry -->
	<div class="modal fade" id="modal-view-entry">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4>{{ selection.selected.title }}</h4>
					<p><small><span am-time-ago="selection.selected.created_at"></span></small></p>
				</div>
				<div class="modal-body preserve-newlines">
					{{ selection.selected.content }}
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-edit-entry" data-dismiss="modal">Edit Entry</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal" ng-click="delete()">Delete Entry</button>
					<button class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal: Edit Entry -->
	<div class="modal fade" id="modal-edit-entry">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4><i class="fa fa-edit"></i> <small>Edit</small> {{ selection.selected.title }}</h4>
					<p><small><span am-time-ago="selection.selected.created_at"></span></small></p>
				</div>
				<div class="modal-body">
					<form onsubmit="return false">
						<textarea class="form-control" placeholder="Write something here..." rows="10" ng-model="selection.selected.content"></textarea>
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary btn-sm" data-toggle="modal" ng-click="update()">Save</button>
					<button class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

</section>
