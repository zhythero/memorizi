<html ng-app="memorizi">
	<head>

		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Dashboard</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootswatch/paper-bootswatch.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/ng-notify.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/ngcloak.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/memorizi.css'); ?>">

		<style>
		body {
			margin-top: 66px;
		}
		</style>

		<script>
			var base_url = '<?php echo base_url(); ?>';
		</script>

		<!-- Angular -->
		<script src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/ng-notify.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/angular-moment.min.js'); ?>"></script>

		<script src="<?php echo base_url('angular/app.js'); ?>"></script>
		<script src="<?php echo base_url('angular/services/userService.js'); ?>"></script>

		<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	</head>
	<body>

		<toast></toast>

		<div class="container-fluid">
				<div class="row">

				<!-- Top Nav  -->
				<script src="<?php echo base_url('angular/controllers/user/navbarController.js'); ?>"></script>

				<nav class="navbar navbar-default navbar-fixed-top" ng-controller="navbarController" ng-init="user.getData()" ng-cloak>
					<div class="container-fluid">

						<div class="navbar-header">
							<a href="#" class="navbar-brand">Memorizi</a>
						</div>

						<!-- Nav left -->
						<ul class="nav navbar-nav navbar-left">
							<li><a href="<?php echo base_url('dashboard/desk'); ?>"><i class="fa fa-pencil fa-fw"></i>My Desk</a></li>
							<li ng-if="user.isAdmin == '1'"><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-user fa-fw"></i>Admin Panel</a></li>
						</ul>

						<!-- Nav right -->
						<ul class="nav navbar-nav navbar-right">
							<p class="navbar-text">
								What's up, <a href="<?php echo base_url('dashboard/account_settings'); ?>">{{ user.username }}</a>?
							</p>
							<li><a href="<?php echo base_url('auth/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
						</ul>

					</div>
				</nav>

				<!-- Content -->
				<div class="col-md-12">
					<header>
						<h3><?php echo ( isset($content_header) ) ? $content_header : ''; ?></h3>
						<p style="color: #999"><em><?php echo ( isset($content_caption) ) ? $content_caption : ''; ?></em></p>
					</header>
					<hr>
