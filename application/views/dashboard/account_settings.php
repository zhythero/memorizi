
<?php if (isset($_SESSION['success'])): ?>
	<div class="alert alert-success">
		<?php echo $_SESSION['success']; ?>
	</div>
<?php endif ?>

<?php if (isset($_SESSION['failed'])): ?>
	<div class="alert alert-danger">
		<?php echo $_SESSION['failed']; ?>
	</div>
<?php endif ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li class="active"><a href="#account" data-toggle="tab">Account</a></li>
</ul>

<div class="well">
	<div class="tab-content">
		
		<!-- Account -->
		<div class="tab-pane active" id="account" style="padding-top: 20px">
			<div class="row">
				<div class="col-md-6">
					<table class="table table-striped">
						<tbody>
							<tr>
								<td>Username:</td>
								<td><?php echo $user_info['username']; ?></td>		
							</tr>
							<tr>
								<td>Password:</td>
								<td><a data-toggle="modal" data-target="#change_password_modal" href="#change_pass">Change password</a></td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>
		</div>

	</div>

</div>

<!-- Change password modal -->
<div class="modal fade" id="change_password_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4><i class="fa fa-key"></i> Change password</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="<?php echo base_url('dashboard/change_password'); ?>" id="change_password_form">
					<div class="form-group">
						<label>Old Password</label>
						<input type="password" class="form-control" name="old_pass" placeholder="Old Password" required>
					</div>
					<div class="form-group">
						<label>New Password</label>
						<input type="password" class="form-control" name="new_pass" placeholder="New Password" required>
					</div>
					<div class="form-group">
						<label>Confirm New Password</label>
						<input type="password" class="form-control" name="new_passconf" placeholder="New password again" required>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" form="change_password_form" class="btn btn-primary">Change</button>
			</div>
		</div>
	</div>
</div>