<html>
	<head>
		<title>Welcome</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
		<style>
			img.img-rounded{
				display: block;
				margin-left: auto;
				margin-right: auto;
				padding-top: 10%;
			}
			.wrap{
				text-align: center;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<img src="assets/img/Memorizi_Logo.png" class="img-rounded">
		</div>
		<div class="wrap">
			<h1>Memorizi</h1>
			<p>Share your memories.</p>
			<p>Read other people's diaries.</p>
			<a href="<?php echo base_url('auth/login'); ?>">Sign in</a> or <a href="<?php echo base_url('auth/register'); ?>">Create an account</a>
		</div>
	</body>
</html>
