<div class="row">

	<div class="col-md-offset-3 col-md-6" style="margin-top: 20vh">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="text-center">Memorizi Login</h3>
			</div>
			<div class="panel-body">
				
				<!-- Validation Errors -->
				<?php if ( !empty( validation_errors() ) ): ?>
					<div class="alert alert-danger">
						<?php echo validation_errors(); ?>
					</div>
				<?php endif; ?>
				
				<form action="" method="POST">
					<label>Username</label><br>
					<input class="form-control" type="text" name="username" placeholder="Username"><br><br>

					<label>Password</label><br>
					<input class="form-control" type="password" name="password" placeholder="Password"><br><br>
					<button class="btn btn-primary" type="submit">Login</button>
					or <a href="<?php echo base_url('auth/register'); ?>">Create an account</a
				</form>
			</div>
		</div>
	</div>
</div>