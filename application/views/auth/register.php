<div class="container" style="margin-top: 10vh">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="register">Register</h3>
				</div>
				<div class="panel-body">
					<!-- Validation Errors -->
					<?php if ( !empty( validation_errors() ) ): ?>
						<div class="alert alert-danger">
							<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>

					<form action="" method="POST">
						<label>Username</label><br>
						<input type="text" class="form-control" placeholder="Username" name="username">

						<label>Name</label><br>
						<input type="text" class="form-control" placeholder="Name" name="name">

						<label>Email</label><br>
						<input type="text" class="form-control" placeholder="Email" name="email">

						<label>Password</label><br>
						<input type="password" class="form-control" placeholder="Your password" name="password">

						<label>Confirm Password</label><br>
						<input type="password" class="form-control" placeholder="Type password again" name="passconf">
						<br>
						
						<center>
						<button class="btn btn-primary" type="submit">Register</button>
						</center>
					</form>
				</div>

				
			</div>
		</div>
	</div>
</div>