<script src="<?php echo base_url('angular/controllers/admin/usersController.js'); ?>"></script>

<section ng-controller="usersController" ng-init="collection.get()" ng-cloak>
	<table class="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th class="text-center">ID</th>
				<th class="text-center">Username</th>
				<th class="text-center">Admin</th>
				<th class="text-center col-md-1"></th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="user in collection.data">
				<td class="text-right">{{ user.id }}</td>
				<td>{{ user.username }}</td>
				<td class="text-center">
					<span ng-if="user.is_admin == '1'" class="text-success">Yes</span>
					<span ng-if="user.is_admin == '0'" class="text-danger">No</span>
				</td>
				<td>
					<button 
						data-toggle="modal"
						data-target="#modal-confirm-delete"
						ng-click="selection.select(user)" 
						class="btn btn-danger btn-block">
							<i class="fa fa-trash"></i>
					</button>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- Modal: Delete User Confirmation -->
	<div class="modal fade" id="modal-confirm-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4>Delete user?</h4>
				</div>
				<div class="modal-body">
					<p>Warning! This action cannot be undone. Are you really sure you want to delete this user?</p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-danger btn-sm" ng-click="delete()" data-dismiss="modal">Yes, delete</button>
					<button class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</section>