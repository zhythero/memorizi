<?php

class DiaryEntry extends CI_Model {

	private $table = 'diary_entries';

	protected $offset = 0;
	protected $limit = 10;
	protected $sort = [
		'field' => 'id',
		'direction' => 'ASC'
	];

	public function set_offset($offset) {
		$this->offset = $offset;
	}

	public function set_limit($limit) {
		$this->limit = $limit;
	}

	public function set_sort($field = 'id', $direction = 'ASC') {
		$this->sort['field'] = $field;
		$this->sort['direction'] = $direction;
	}

	public function get($fields = NULL, $where = NULL, $like = NULL) {

		// Select
		if (!is_null($fields)) {
			if (is_array($fields))
				foreach ($fields as $field)
					$this->db->select($field);
			else if (is_string($fields))
				$this->db->select($fields);
		}

		// Where
		if (!is_null($where)) {
			foreach ($where as $field => $value) {
				$this->db->where($field, $value);
			}
		}

		// Like
		if (!is_null($like)) {
			foreach ($like as $field => $value) {
				$this->db->like($field, $value);
			}
		}

		$this->db->order_by($this->sort['field'], $this->sort['direction']);

		$result = $this->db->get($this->table, $this->limit, $this->offset);

		return $result->result_array();

	}

	public function set($key_value_pairs, $where) {

		foreach ($where as $key => $value)
			$this->db->where($key, $value);

		return $this->db->update($this->table, $key_value_pairs);

	}

	public function create($data) {
		return $this->db->insert($this->table, $data);
	}

	public function delete($diary_entry_id) {
		$this->db->where('id', $diary_entry_id);
		$this->db->delete( $this->table );
	}

}

 ?>
