<?php 

class User extends CI_Model {

	private $table = 'users';

	private $offset = 0;
	private $limit = 10;

	public function set_offset($offset) {
		$this->offset = $offset;
	}

	public function set_limit($limit) {
		$this->limit = $limit;
	}

	public function get($fields = NULL, $where = NULL) {

		if (!is_null($fields)) {
			if (is_array($fields))
				foreach ($fields as $field)
					$this->db->select($field);
			else if (is_string($fields))
				$this->db->select($fields);
		}

		if (!is_null($where)) {
			foreach ($where as $field => $value) {
				$this->db->where($field, $value);
			}
		}

		$result = $this->db->get($this->table, $this->limit, $this->offset);

		return $result->result_array();

	}

	public function set($key_value_pairs, $where) {

		foreach ($where as $key => $value)
			$this->db->where($key, $value);

		return $this->db->update($key_value_pairs);

	}

	public function delete($user_id) {
		$this->db->where('id', $user_id);
		$this->db->delete($this->table);
	}

	public function authenticate($user, $pass) {

		$pass = md5($pass);

		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		$result = $this->db->get($this->table);

		return ($result->num_rows() == 1) ? true : false;
	}

	public function validate_username($username) {

		// Check for existing username
		$this->db->where('username', $username);
		$result = $this->db->get($this->table);
		
		return ($result->num_rows() < 1) ? true : false;

	}

	public function register($data) {
		return $this->db->insert($this->table, [
				'username' => $data['username'],
				'password' => $data['password']
			]);
	}

	public function change_password($user_id, $password) {

		$this->db->where('id', $user_id);
		return $this->db->update($this->table, ['password' => md5($password)]);

	}
}

 ?>