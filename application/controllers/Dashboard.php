<?php 

class Dashboard extends CI_Controller {

	public function index() {
		
		only_logged_in();

		redirect('dashboard/desk');

	}

	public function account_settings() {

		only_logged_in();
		
		$this->load->model('User');

		$data['content_header'] = "<i class='fa fa-wrench'></i> Account Settings";
		$data['user_info'] = $this->User->get(NULL, ['id' => $_SESSION['user_id']])[0];

		$this->load->view('dashboard/_head', $data);
		$this->load->view('dashboard/account_settings');
		$this->load->view('dashboard/_foot');
	}

	public function desk() {

		only_logged_in();

		$data['content_header'] = "<i class='fa fa-pencil'></i> My Desk";
		$data['content_caption'] = "How are you today?";

		$this->load->view('dashboard/_head', $data);
		$this->load->view('dashboard/desk');
		$this->load->view('dashboard/_foot');
	}

	public function change_password() {

		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_rules('old_pass', 'Old Password', 'required|callback_validate_current_password', ['validate_current_password' => 'Wrong password.']);
		$this->form_validation->set_rules('new_pass', 'New Password', 'required|min_length[8]');
		$this->form_validation->set_rules('new_passconf', 'New Password Confirmation', 'required|matches[new_pass]');

		if ($this->form_validation->run()) {

			$this->load->model('User');
			$this->User->change_password($_SESSION['user_id'], $_POST['new_pass']);

			$this->session->set_flashdata('success', 'Successfully changed password.');
		}
		else
			$this->session->set_flashdata('failed', validation_errors());

		redirect('dashboard/account_settings');
	}


	public function validate_current_password() {

		$this->load->model('User');

		$username = $this->User->get('username', ['id' => $_SESSION['user_id']])[0]['username'];

		return $this->User->authenticate($username, $_POST['old_pass']);
	}


}

 ?>