<?php 

class Admin extends CI_Controller {

	public function index() {

		only_admins();
		redirect('admin/users');

	}

	public function users() {

		$data['content_header'] = "<i class='fa fa-users fa-fw'></i> Users";

		$this->load->view('admin/_head', $data);
		$this->load->view('admin/users');
		$this->load->view('admin/_foot');

	}

}

 ?>