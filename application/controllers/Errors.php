<?php 

class Errors extends CI_Controller {

	public function index() {
		redirect('errors/page_not_found');
	}

	public function page_not_found() {
		show_404();
	}
}

 ?>