<?php

header('Content-Type: application/json');

class User_Service extends CI_Controller {

	public function index() {
		$this->send_response(false, 'No method');
	}

	private function send_response($success, $response = '') {
		echo json_encode([
				'success' => $success,
				'data' => $response
			], JSON_PRETTY_PRINT);

		exit;
	}

	private function verify_session() {
		if (!isset($_SESSION['user_id'])) {
			$this->send_response(false, 'Unauthenticated');
			exit;
		}
	}

	public function user($method = 'me') {

		$this->verify_session();
		$this->load->model('User', 'user');

		switch ($method) {
			case 'me':
				$me = $this->user->get(['id', 'username', 'is_admin'], [
						'id' => $_SESSION['user_id']
					]);
				$this->send_response(true, $me[0]);
				break;
		}
	}

	public function diary_entries($method = 'get', $second_param = '') {

		$this->verify_session();
		$this->load->model('DiaryEntry', 'diary_entries');

		switch ($method) {
			case 'get':

				if ( ! empty($second_param) ) {

					$diary_entry = $this->diary_entries->get(NULL, [
							'user_id' => $_SESSION['user_id'],
							'id' => $second_param
						]);

					if (empty($diary_entry)) {
						$this->send_response(false, 'Diary Entry not found');
					} else {
						$this->send_response(true, $diary_entry[0]);
					}

				} else {
					$this->diary_entries->set_sort('id', 'DESC');
					$collection = $this->diary_entries->get(NULL, [
							'user_id' => $_SESSION['user_id']
						]);
					$this->send_response(true, $collection);
				}

				break;

			case 'update':

				$this->load->library('form_validation');

				$this->form_validation->set_rules('diary_entry_id', 'Diary Entry ID', 'required');
				$this->form_validation->set_rules('content', 'Content', 'required');

				if ($this->form_validation->run()) {

					$data = [
							'content' => $_POST['content']
						];

					$where = ['id' => $_POST['diary_entry_id']];

					$this->diary_entries->set($data, $where);

					$this->send_response(true);
				} else
					$this->send_response(false, $this->form_validation->error_string(' ', ' '));

				break;

			case 'publish':

				$this->load->library('form_validation');

				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('content', 'Content', 'required');

				if ($this->form_validation->run()) {

					$this->diary_entries->create([
							'title' => $_POST['title'],
							'content' => $_POST['content'],
							'user_id' => $_SESSION['user_id']
						]);

					$this->send_response(true);
				} else
					$this->send_response(false, $this->form_validation->error_string(' ', ' '));

				break;

			case 'delete':

				if ( !isset($_POST['diary_entry_id']) ) {
					$this->send_response(false, 'No diary entry passed');
				} else {
					$this->diary_entries->delete($_POST['diary_entry_id']);
					$this->send_response(true);
				}

				break;

			default:
				$this->send_response(false, 'Invalid method');
		}
	}

}

 ?>
