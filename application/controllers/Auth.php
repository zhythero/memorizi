<?php 

class Auth extends CI_Controller {

	public function index() 
	{
		redirect('auth/login');
	}

	public function login()
	{

		only_guests();

		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_authenticate', ['authenticate' => 'Incorrect username/password.']);

		if ($this->form_validation->run()) {

			$this->session->set_userdata( 'user_id', $this->User->get(['id'], ['username' => $_POST['username']])[0]['id'] );
			$this->session->set_userdata( 'is_admin', $this->User->get(['is_admin'], ['username' => $_POST['username']])[0]['is_admin'] );
			
			redirect('dashboard');

		} else {
			$this->load->view('auth/_head');
			$this->load->view('auth/login');
			$this->load->view('auth/_foot');
		}

	}

	public function logout() 
	{
		$this->session->unset_userdata('user_id');
		redirect('auth/login');
	}

	public function register() 
	{

		only_guests();

		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->form_validation->set_rules('username', 'Username', 'required|callback_validate_username', ['validate_username' => 'Username exists or invalid.']);
		$this->form_validation->set_rules('password', 'Password', 'required|md5');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|md5|matches[password]');

		if ($this->form_validation->run()) {

			$this->load->model('User');
			unset($_POST['passconf']);
			$result = $this->User->register($_POST);

			if ($result)
				$this->session->set_flashdata('success', ['Successfully created new diary entry.']);

			redirect('auth/login');

		} else {
			$this->load->view('auth/_head');
			$this->load->view('auth/register');
			$this->load->view('auth/_foot');
		}
	}

	// callback_authenticate
	public function authenticate() 
	{
		$this->load->model('User');
		return $this->User->authenticate($_POST['username'], $_POST['password']);
	}

	// callback_validate_username
	public function validate_username() 
	{
		$this->load->model('User');
		return $this->User->validate_username($_POST['username']);
	}

}

 ?>