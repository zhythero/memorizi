memorizi.controller('usersController', function($scope, adminService) {

	$scope.collection = {
		offset : 0,
		limit : 20,
		data : [],
		get : function() {
			adminService.get('users')
				.then(function(response) {
					if (response.success){
						$scope.collection.data = $scope.collection.data.concat(response.data)
					}
				});
		},
		reget : function() {
			this.data = [];
			adminService.get('users')
				.then(function(response) {
					if (response.success){
						$scope.collection.data = $scope.collection.data.concat(response.data)
					}
				});
		}
	}

	$scope.selection = {
		selected : {},
		select : function(user) {
			this.selected = user;
		}
	}

	$scope.delete = function() {

		adminService
			.post('users/delete', {
				user_id : $scope.selection.selected.id
			})
			.then(function(response) {
				console.log(response);
				$scope.collection.reget();
			});

	}

});