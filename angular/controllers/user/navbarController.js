memorizi.controller('navbarController', function($scope, userService) {

	$scope.user = {
		id : -1,
		username : '',
		isAdmin : false,
		getData : function () {
			userService
				.get('user/me')
				.then(function(response) {

					if (response.success) {
						$scope.user.id = response.data.id;
						$scope.user.username = response.data.username;
						$scope.user.isAdmin = response.data.is_admin;
					}

				});
		}
	}

});