memorizi.controller('diaryEntriesController', function($scope, userService, ngNotify) {

	$scope.collection = {
		offset : 0,
		limit : 20,
		data : [],
		get : function() {
			userService.get('diary_entries')
				.then(function(response) {
					if (response.success){
						$scope.collection.data = $scope.collection.data.concat(response.data)
					}
				});
		},
		reget : function() {
			this.data = [];
			userService.get('diary_entries')
				.then(function(response) {
					if (response.success){
						$scope.collection.data = $scope.collection.data.concat(response.data)
					}
				});
		}
	}

	$scope.selection = {
		selected : {},
		select : function(diaryEntry) {
			this.selected = diaryEntry;
		}
	}

	$scope.update = function() {

		userService
			.post('diary_entries/update', {
				diary_entry_id : $scope.selection.selected.id,
				content : $scope.selection.selected.content
			})
			.then(function(response) {

				if (response.success) {
					ngNotify.set('Successfully updated diary entry.', 'success');
				} else {
					ngNotify.set(response.data, 'error');
				}

				$scope.collection.reget();
			});
	}

	$scope.delete = function() {

		userService
			.post('diary_entries/delete', {
				diary_entry_id : $scope.selection.selected.id
			})
			.then(function(response) {
				if (response.success) {
					console.log('deletion success');
				} else {
					console.log('something went wrong');
				}

				$scope.collection.reget();
			});

	}

	$scope.compose = {
		data : {
			title : '',
			content : ''
		},
		response : {
			message : '',
			success : false
			},
		publishing : false,
		publish : function() {

			this.publishing = true;

			userService
				.post('diary_entries/publish', $scope.compose.data)
				.then(function(response) {

					$scope.compose.publishing = false;

					if (response.success) {
						$scope.compose.response.message = 'Successfully published!';
						$scope.compose.response.success = true;
						$scope.compose.data.title = '';
						$scope.compose.data.content = '';
						$scope.collection.reget();
					} else {
						$scope.compose.response.message = response.data;
						$scope.compose.response.success = false;
					}
				}
			);
		}
	}

});
