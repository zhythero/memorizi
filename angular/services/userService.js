memorizi.service('userService', ['$http', function($http) {

    this.get = function(url, param)  {
        return $http.get(base_url + '/User_Service/' + url, param).then(function(response) {
            return response.data;
        });
    };

    this.post = function(url, param) {
        return $http.post(base_url + '/User_Service/' + url, param).then(function(response) {
            return response.data;
        });
    };

}]);